{
double lwd=0.0;

gStyle->SetOptStat(0);

TCanvas* c4 = new TCanvas("c4","c4",800,600);

//c4->SetLogy();
//c4->SetLogx();

leg = new TLegend(0.4,0.6,0.89,0.89);
leg->SetLineColor(0);
leg->SetFillColor(0);


//const char* hname="h3;1";
//const char* hname="E_C_2_2;1";
//const char* hname="E_C_2_3";
//const char* hname="pT_C_2_3;1";
const char* hname="E_C_2_3;1";
//const char* hname="pT_C_0.1_3;1";

TFile f1("../cp_qq.lhe/test.root");
TH2D *g1a = (TH2D*)f1.Get(hname);
TH1D *g1 = g1a->ProjectionX("g1");
//g1-> Rebin(8);
Double_t scale = 1/g1->Integral();
g1->Scale(scale);
g1->SetLineColor(kRed);
g1->SetLineWidth(3);
g1->SetLineStyle(1);

 g1->GetYaxis()->SetTitle("#frac{1}{N} #frac{dN}{dC} [arb. units]");


g1->GetYaxis()->SetTitleFont(62);
g1->GetXaxis()->SetTitleFont(62);
g1->GetYaxis()->SetTitleSize(0.05);
g1->GetXaxis()->SetTitleSize(0.05);
g1->GetYaxis()->SetTitleOffset(1.5);
g1->GetYaxis()->SetLabelFont(62);
g1->GetXaxis()->SetLabelFont(62);
g1->GetYaxis()->SetLabelSize(0.035);
g1->GetXaxis()->SetLabelSize(0.035);

g1->SetMinimum(1e-11);
//g1->Fit("pol9");

leg->AddEntry(g1,"Coloron cp #to qq","l");
g1.Draw();

//g1.Draw();
   c4-> Update();


//.... second file
TFile f2("../zp_qq.lhe/test.root");
TH2D *g2a = (TH2D*)f2.Get(hname);
TH1D* g2 = g2a->ProjectionX("g2"); 
//g2-> Rebin(8);
Double_t scale = 1/g2->Integral();
g2->Scale(scale);
g2->SetLineColor(6);
g2->SetLineWidth(3);
g2->SetLineStyle(2);
//g2->Fit("pol9");

leg->AddEntry(g2,"Z' zp #to qq","l");

   g2.Draw("same");

//   g2.Draw("same");
   c4-> Update();
//   g2.Draw("same");
//... Third file

/*
//....third file
TFile f3("../diquark_250_j.lhe/test.root");
TH2D *g3a = (TH2D*)f3.Get(hname);
TH1D * g3 = g3a-> ProjectionX("g3");
//g3-> Rebin(8);
Double_t scale = 1/g3->Integral();
g3->Scale(scale);
g3->SetLineColor(1);
g3->SetLineWidth(3);
g3->SetLineStyle(3);
//g2->Fit("pol9");
//
//leg->AddEntry(g3,"#Phi_{6} (M=250 GeV)","l");
//
//   g3.Draw("same");
//
//   //   g2.Draw("same");
//      c4-> Update();
//

//....third fil
TFile f4("../xquark_250_j.lhe/test.root");
TH2D *g4a = (TH2D*)(f4.Get(hname));
TH1D *g4 = g4a-> ProjectionX("g4");
//g4-> Rebin(8);
Double_t scale = 1/g4->Integral();
g4->Scale(scale);
g4->SetLineColor(6);
g4->SetLineWidth(3);
g4->SetLineStyle(4);
////g2->Fit("pol9");
////
//leg->AddEntry(g4,"q^{*} (M=250 GeV)","l");
////
//   g4.Draw("same");
*/

leg->Draw();
}
//#Delta#phi^{h}(l^{+},l^{-})
//#Delta#phi(t,#bar{t})
