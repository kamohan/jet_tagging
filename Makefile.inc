# PYTHIA configuration file.
# Generated on Tue Jan 10 11:21:20 EST 2017 with the user supplied options:
# --prefix=/home/kirtimaan/hepsoft/pythia_install
# --with-hepmc2=/home/kirtimaan/hepsoft/hepmc_install
# --with-root=/opt/root5
# --with-lhapdf6=/home/kirtimaan/hepsoft/lhapdf_install
# --with-boost
# --with-gzip
# --with-root-lib=/opt/root5/lib/root
# --with-root-include=/opt/root5/include/root
# --with-fastjet3=/home/kirtimaan/hepsoft/fastjet_install

# Install directory prefixes.
PREFIX_BIN=/home/kirtimaan/hepsoft/pythia_install/bin
PREFIX_INCLUDE=/home/kirtimaan/hepsoft/pythia_install/include
PREFIX_LIB=/home/kirtimaan/hepsoft/pythia_install/lib
PREFIX_SHARE=/home/kirtimaan/hepsoft/pythia_install/share/Pythia8

# Compilation flags (see ./configure --help for further documentation).
ENABLE_SHARED=false
CXX=g++
CXX_COMMON=-O2 -ansi -pedantic -W -Wall -Wshadow -fPIC
CXX_SHARED=-shared
CXX_SONAME=-Wl,-soname
LIB_SUFFIX=.so

# EVTGEN configuration.
EVTGEN_USE=false
EVTGEN_BIN=
EVTGEN_INCLUDE=./
EVTGEN_LIB=./

# FASTJET3 configuration.
FASTJET3_USE=true
FASTJET3_BIN=/home/kirtimaan/hepsoft/fastjet_install/bin/
FASTJET3_INCLUDE=/home/kirtimaan/hepsoft/fastjet_install/include
FASTJET3_LIB=/home/kirtimaan/hepsoft/fastjet_install/lib

# HEPMC2 configuration.
HEPMC2_USE=true
HEPMC2_BIN=/home/kirtimaan/hepsoft/hepmc_install/bin/
HEPMC2_INCLUDE=/home/kirtimaan/hepsoft/hepmc_install/include
HEPMC2_LIB=/home/kirtimaan/hepsoft/hepmc_install/lib

# HEPMC3 configuration.
HEPMC3_USE=false
HEPMC3_BIN=
HEPMC3_INCLUDE=./
HEPMC3_LIB=./

# LHAPDF5 configuration.
LHAPDF5_USE=false
LHAPDF5_BIN=
LHAPDF5_INCLUDE=./
LHAPDF5_LIB=./
LHAPDF5_PLUGIN=LHAPDF5.h

# LHAPDF6 configuration.
LHAPDF6_USE=true
LHAPDF6_BIN=/home/kirtimaan/hepsoft/lhapdf_install/bin/
LHAPDF6_INCLUDE=/home/kirtimaan/hepsoft/lhapdf_install/include
LHAPDF6_LIB=/home/kirtimaan/hepsoft/lhapdf_install/lib
LHAPDF6_PLUGIN=LHAPDF5.h

# POWHEG configuration.
POWHEG_USE=false
POWHEG_BIN=
POWHEG_INCLUDE=./
POWHEG_LIB=./

# PROMC configuration.
PROMC_USE=false
PROMC_BIN=
PROMC_INCLUDE=./
PROMC_LIB=./

# ROOT configuration.
ROOT_USE=true
ROOT_BIN=/opt/root5/bin/
ROOT_INCLUDE=/opt/root5/include/root
ROOT_LIB=/opt/root5/lib/root

# GZIP configuration.
GZIP_USE=true
GZIP_BIN=
GZIP_INCLUDE=./
GZIP_LIB=./

# BOOST configuration.
BOOST_USE=true
BOOST_BIN=
BOOST_INCLUDE=./
BOOST_LIB=./

# PYTHON configuration.
PYTHON_USE=false
PYTHON_BIN=
PYTHON_INCLUDE=./
PYTHON_LIB=./
