#######################################
#######################################
The codes in this repository:
1.jetpy.cc : python code that reads in lhe(.gz) files and produces JET energy correlations for fatjets
2.plotters > draw_new.c reads root files and draws graphs.
3. input.cmnd > pythia options
4. runsscript.sh > script to run code over all .lhe.gz files in a given directory
5. Makefile > update the library paths 
