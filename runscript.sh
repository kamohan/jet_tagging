#!/bin/bash
make clean
make

evtf="event_files/"
uwtf="unweighted_events.lhe.gz"
pyan="jetpy"
inp="input.cmnd"
exp="Beams:LHEF ="
exp2="$exp ..\/"


for f in `ls -d $evtf*`
do

 fname=$f
# fname=$f
 dname=${f#$evtf}
 dname=${dname%_$uwtf}
 echo $fname
 if [ -e $f ]
  then
  echo "Found file : $f. Starting run... " 
  rm -rf $dname
  mkdir $dname
  cp $pyan $inp $dname
  expr2="$exp2$f"
#  expr2="$f"
  cmd1="sed -i  '/$exp/c\\$expr2' $dname/$inp"
  #echo $cmd1
  eval $cmd1
 
  cd $dname 
  nohup ./$pyan >> $dname.log 2>&1 &

 fi
 cd ../

done
