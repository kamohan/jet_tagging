// Programme to calculate jet energy corrleators

// Includes basic detector simulation inspired by delphes
// Orginally written by K. Mohan. Various parts updated by D.Sengupta
// Input using ATLAS tune 4C as input.cmnd

#include <iomanip>
#include <stdlib.h>
#include <stdio.h>
#include <ctime>
#include <iostream>
#include <istream>
#include <fstream>
#include <sstream>
#include <string>
#include <algorithm> // for std::find
#include <iterator> // for std::begin, std::end
#include <time.h>


#include "Pythia8/Pythia.h"
#include "Pythia8Plugins/CombineMatchingInput.h"

// ROOT, for histogramming.
#include "TH1.h"
#include "TH2.h"
#include "TH3.h"

// ROOT, for interactive graphics.
#include "TVirtualPad.h"
#include "TApplication.h"

// ROOT, for saving file.
#include "TFile.h"

#include "TF1.h"
#include "TMath.h"
#include "TString.h"
#include "TFormula.h"
#include "TRandom3.h"



// Includes for Fastjet

#include "fastjet/PseudoJet.hh"
#include "fastjet/ClusterSequence.hh"
#include "fastjet/JetDefinition.hh"


#include "fastjet/contrib/EnergyCorrelator.hh" // In external code, this should be fastjet/contrib/EnergyCorrelatoir.hh

#include "fastjet/tools/MassDropTagger.hh"
#include "fastjet/tools/Filter.hh"



using namespace Pythia8;
using namespace fastjet;
using namespace fastjet::contrib;

//********************************************************************//
// Log bins for plots
//********************************************************************//
void BinLogX(TH1*h) 
{

   TAxis *axis = h->GetXaxis(); 
   int bins = axis->GetNbins();

   Axis_t from =TMath::Log10(axis->GetXmin());
   Axis_t to = TMath::Log10(axis->GetXmax());
   Axis_t width = (to - from) / bins;
   Axis_t *new_bins = new Axis_t[bins + 1];
   

   for (int i = 0; i <= bins; i++) {
     new_bins[i] = TMath::Power(10, from + i * width);
   } 
   axis->Set(bins, new_bins); 
   delete new_bins; 
}
//********************************************************************//
// Is a Jet Constituent 
/// this predicate returns true if the input particle should be clustered in Jet
//********************************************************************//
class IsJetC {
public:
    bool operator()( const Pythia8::Particle* p ) {
	if(p->isFinal() && p->pT() > 0.5 && fabs(p->eta()) < 4.0 )
	{
		int pid = p->id();
		//std::cout<<"pid = "<<pid <<" pT= "<<p->pT()<<endl;
		int lepArray[5] ={11,13};
		int neuArray[5] ={12,14,16};
		bool isNeu = std::any_of(std::begin(neuArray), std::end(neuArray), [&](int i)
		{
			return i == abs(pid);
		});
		bool isLep = std::any_of(std::begin(lepArray), std::end(lepArray), [&](int i)
		{
			return i == abs(pid);
		});
		if((isLep && p->pT() > 5.) || isNeu  ) return 0;
				
		return 1;
        }
        return 0;
    }
};
//********************************************************************//
//*************************************//
typedef vector<double> Row; // One row of the matrix
typedef vector<Row> Matrix; // Matrix: a vector of rows
typedef vector<Matrix> Mat3d; // Matrix: a vector of rows

typedef vector<TH2D*> HRow; // One row of the matrix
typedef vector<HRow> HMatrix; // Matrix: a vector of rows
typedef vector<HMatrix> HMat3d; // Matrix: a vector of rows
//*************************************//
//********************************************************************//
//********************************************************************//
//=== For constructing anti_kt fat jets
//********************************************************************//
PseudoJet fatjets(const vector<PseudoJet> & input_particles, PseudoJet & resonance){
	// Initial clustering with anti-kt algorithm
	JetAlgorithm algorithm = antikt_algorithm;
	double jet_rad = 1.0; // jet radius for anti-kt algorithm
	JetDefinition jetDef = JetDefinition(algorithm,jet_rad,E_scheme,Best);
	ClusterSequence clust_seq(input_particles,jetDef);
	vector<PseudoJet> antikt_jets  = sorted_by_pt(clust_seq.inclusive_jets());
	PseudoJet finjet;
double dR=9999.0;
	for (int j = 0; j < antikt_jets.size(); j++) { 
                
                double dR2= resonance.delta_R(antikt_jets[j]);
		if( dR2 < dR)
		{           
		finjet=antikt_jets[j]; dR= dR2;
                
                }

}
return finjet;
}
//********************************************************************//
//========For calculatng energy correlators
//********************************************************************//
Mat3d Ecorel( const vector<PseudoJet> & input_particles,  PseudoJet & resonance) {

	//JetAlgorithm algorithm = antikt_algorithm;
	JetAlgorithm algorithm = cambridge_algorithm;
	double jet_rad = 1.0; // jet radius for anti-kt algorithm
	JetDefinition jetDef = JetDefinition(algorithm,jet_rad,E_scheme,Best);
	ClusterSequence clust_seq(input_particles,jetDef);
	vector<PseudoJet> antikt_jets  = sorted_by_pt(clust_seq.inclusive_jets());
  

//====== EnergyCorrelator ====================//
//======= various values of beta ==============//
	vector<double> betalist;
	betalist.push_back(0.1);
	betalist.push_back(0.2);
	betalist.push_back(0.5);
	betalist.push_back(1.0);
	betalist.push_back(1.5);
	betalist.push_back(2.0);

//==== checking the two energy/angle modes=======//
vector<EnergyCorrelator::Measure> measurelist;
measurelist.push_back(EnergyCorrelator::pt_R);
measurelist.push_back(EnergyCorrelator::E_theta);
//measurelist.push_back(EnergyCorrelator::E_inv);
	
//========Store correlators==================//
  Mat3d mat_corels(measurelist.size(), Matrix(betalist.size(), Row(5))); // Declare size

//====Decalre objects on which correlator is run
  PseudoJet myJet, myJeti;

	double dR=9999.0;
	for (int j = 0; j < antikt_jets.size(); j++) { 
                
                double dR2= resonance.delta_R(antikt_jets[j]);
//		cout << j << " dr2 = " << dR2 << " dr =" << dR << endl;
		if( dR2 < dR)
		{           
//			cout<<"if dR = " << dR <<" dR2  =" <<dR2<<endl;
		myJeti=antikt_jets[j]; dR= dR2;
                }        
//                         cout << " resonance.phi= " << resonance.phi() <<"antikt_jets[].phi= "<<antikt_jets[j].phi()<<endl;
}
//cout<<"myJet.phi= "<<myJet.phi()<<endl;
//cout<<"=============="<<endl;

        //fastjet::Filter trimmer(fastjet::JetDefinition(fastjet::kt_algorithm,0.3),fastjet::SelectorPtFractionMin(0.05));
        fastjet::Filter trimmer(fastjet::JetDefinition(fastjet::kt_algorithm,0.3),fastjet::SelectorPtFractionMin(0.00));
        PseudoJet trimmed = trimmer(myJeti);
/*	vector <PseudoJet> kept = trimmed.pieces();
        cout<<"size of trimmed jets = "<< kept.size()<<endl;
	ClusterSequence clust_seq2(kept,jetDef);
        vector<PseudoJet>myJet3 = sorted_by_pt(clust_seq2.inclusive_jets());*/
        myJet=trimmed;

        cout<<"size of trimmed jets = "<< trimmed.pieces().size()<<endl;


  vector<string> modename;
  modename.push_back("pt_R");
  modename.push_back("E_theta");

			for (unsigned int M = 0; M < measurelist.size(); M++) {
				//cout << "-------------------------------------------------------------------------------------" << endl;
				//cout << "EnergyCorrelatorDoubleRatio:  C_N^(beta) = r_N^(beta)/r_{N-1}^(beta) with " << modename[M] << endl;
				//cout << "-------------------------------------------------------------------------------------" << endl;
				//printf("%7s %14s %14s %14s %14s \n","beta", "N=1", "N=2", "N=3", "N=4");

				for (unsigned int B = 0; B < betalist.size(); B++) {
					double beta = betalist[B];

					EnergyCorrelatorDoubleRatio C1(1,beta,measurelist[M]);
					EnergyCorrelatorDoubleRatio C2(2,beta,measurelist[M]);
					EnergyCorrelatorDoubleRatio C3(3,beta,measurelist[M]);
					//EnergyCorrelatorDoubleRatio C4(4,beta,measurelist[M]);

					
					mat_corels[M][B][0]=beta;
					mat_corels[M][B][1]=C1(myJet);
					mat_corels[M][B][2]=C2(myJet);
					mat_corels[M][B][3]=C3(myJet);
					mat_corels[M][B][4]=0;//C4(myJet);
//					cout << "C1= " << C1(myJet) << endl;
					

				}

			}


	return mat_corels;
}


//********************************************************************//
//==== BDRS tagger for checking the mass criteria for the resonance===///
//********************************************************************//
PseudoJet MDTAG(const vector<PseudoJet> & input_particles) 
{
	
	PseudoJet resonance(0,0,0,0);
	
	JetAlgorithm algorithm = cambridge_algorithm;
	double jet_rad = 1.0; // jet radius for anti-kt algorithm
	JetDefinition jetDef = JetDefinition(cambridge_algorithm, jet_rad);
	ClusterSequence clust_seq(input_particles,jetDef);
	vector<PseudoJet> outputList  = sorted_by_pt(clust_seq.inclusive_jets());
	//			cout<<"This is where to add new stuff"<<endl;
	double HTSUM=0.0;
        for(int i=0; i < outputList.size(); i++){
        HTSUM += outputList[i].pt();
	}			
	//if(HTSUM < 900){resonance.reset(0,0,0,0); return resonance ;}
	if(outputList[0].pt()<100){resonance.reset(0,0,0,0); return resonance ;}
	
	double mucut=0.67;
	double ycut=0.1;
	PseudoJet jets2=outputList[0];
	PseudoJet jets3, tagged2;
	PseudoJet tagged;
	//double ptjetcut=200.0;
	double bjetptcut=20.0;
	double bjetetacut=2.5; 
	//if(jets2.perp()<ptjetcut){outputList.clear();return;}
	// now do jet tagging using a mass drop tagger
	MassDropTagger md_tagger(mucut, ycut);
	PseudoJet tagged1 = md_tagger(jets2);
	if(outputList.size()>1){
		jets3=outputList[1];
		tagged2=md_tagger(jets3);
	}
	// Delphes includes hard leptons in jet reconstruction 
	// This step is to avoid using the leptons in filtering procedure
	if(tagged1 == 0 && tagged2 !=0){tagged = tagged2; }
	else if(tagged1 !=0 && tagged2 !=0){
	   if((tagged1.constituents()).size() < (tagged2.constituents()).size()){
		   tagged=tagged2;
	   }
	   else{
		   tagged=tagged1;
	   }
	}
	else{tagged=tagged1;}
		   

	if (tagged == 0){
	//     cout << "No substructure found" << endl;
	  outputList.clear();
		resonance.reset(0,0,0,0); return resonance ;
	}

	PseudoJet parent1 = tagged.pieces()[0];
	PseudoJet parent2 = tagged.pieces()[1];


	// next we "filter" it, to remove UE & pileup contamination
	double   Rbb = parent1.delta_R(parent2);
	double   Rfilt = min(Rbb/2., 0.3); // somewhat arbitrary choice
	unsigned nfilt = 3;               // number of pieces we'll take
	//   cout << "Subjet separation (Rbb) = " << Rbb << ", Rfilt = " << Rfilt << endl;

	Filter filter(JetDefinition(cambridge_algorithm, Rfilt),SelectorNHardest(nfilt));
	PseudoJet filtered = filter(tagged);

	Selector select_rapidity = SelectorAbsRapMax(bjetetacut);
	Selector select_pt = SelectorPtMin(bjetptcut);
	Selector select_both = select_pt && select_rapidity;
	vector<PseudoJet> selected_jets = select_both(filtered.pieces());



	if (selected_jets.size()<2){outputList.clear();resonance.reset(0,0,0,0); return resonance ;}

	outputList.clear();
	outputList = sorted_by_pt(selected_jets);
	
	resonance = outputList[0];
	for(unsigned int ii = 1; ii< outputList.size(); ii++){
		resonance=join(resonance,outputList[ii]);
	}
		
	
	return resonance;
}
//********************************************************************//
//********************************************************************//
//============================JET SMEARING ===========================//
//********************************************************************//
//********************************************************************//
vector<PseudoJet> SmearedJets(const vector<PseudoJet> & input_particles){
	//First cluster 0.1 R Jets for granularity
	JetAlgorithm algorithm = antikt_algorithm;
	double jet_rad = 0.1; // jet radius for anti-kt algorithm
	JetDefinition jetDef = JetDefinition(algorithm, jet_rad);
	ClusterSequence clust_seq(input_particles,jetDef);
	vector<PseudoJet> subjets  = sorted_by_pt(clust_seq.inclusive_jets());
	vector<PseudoJet> smearjet;
	double pttemp =0; double etatemp=0; double mean=0; double pxtemp =0; double pytemp =0; double etemp = 0; double deltar=0;
	// Now smear the jets.
	for (unsigned int i= 0; i< subjets.size(); i++)
	{
		pttemp = subjets[i].pt() ;
	        etatemp = subjets[i].eta() ;
	// Smearing Jets               
	if (abs(etatemp) <= 1.5) {
		if(pttemp > 0.1 && pttemp < 1   )
		{
			mean = 0.02*pttemp ;
		 }
		else if (pttemp > 1 && pttemp < 10 ) 
		{
			mean = 0.01*pttemp;
		}
		else if (pttemp > 10 && pttemp < 200  )
		{
			mean = 0.03*pttemp;
		}
		else if (pttemp > 200  ){
			mean = 0.05*pttemp;
		}
	}

	if (abs(etatemp) > 1.5 && abs(etatemp) <= 2.5  ) { 
		if ( (pttemp > 0.1 && pttemp < 1) )
		{
			mean =0.03*pttemp;
		}
		else if (pttemp > 1 && pttemp < 10   )
		{
			mean = 0.02*pttemp;
		}
		else if ( pttemp > 10 && pttemp < 200   )
		{
			mean =0.04*pttemp;
		}
		else if ( pttemp > 200  ){
			mean =0.05*pttemp;
		}
	}
				
	pttemp = gRandom->Gaus(pttemp, mean);
	pxtemp = gRandom->Gaus( subjets[i].px(), mean);
	pytemp = gRandom->Gaus( subjets[i].py(), mean);
	etemp =  gRandom->Gaus( subjets[i].e(), mean);
	PseudoJet jetpart(pxtemp,pytemp,subjets[i].pz(),etemp);
	smearjet.push_back(jetpart);
	
	}

	return smearjet;


}



//********************************************************************//
//********************************************************************//
//************************************************************//
//************************************************************//
//************************************************************//
//************************************************************//
// Main 

//************************************************************//
//************************************************************//
//************************************************************//
//************************************************************//
//********************************************************************//
//********************************************************************//
int main() {
	
	const char* outname="test.root";
	TFile f(outname,"RECREATE");
	// Book histograms
	//  gStyle->SetOptStat(0);
	
	double llim=0; double ulim = 1.5;
        double ptmin=100; double ptmax = 2000; 
	
	HMat3d h_corels(2, HMatrix(6, HRow(5)));
	
	// various values of beta
	vector<double> betalist;
	betalist.push_back(0.1);
	betalist.push_back(0.2);
	betalist.push_back(0.5);	
	betalist.push_back(1.0);
	betalist.push_back(1.5);
	betalist.push_back(2.0);
	
	std::string h_name_pref="C";
	std::string s_measure[2]={"E","pT"};
	for (unsigned int M = 0; M < 2; M++) {
		for (unsigned int B = 0; B < betalist.size(); B++) {
			for (unsigned int C = 1; C < 5; C++) {
		
		std::stringstream s;
		s<<s_measure[M]<<"_"<<h_name_pref<<"_"<<betalist[B]<<"_"<<C;
		std::string s2= s.str();
		const char* c2=s2.c_str();
		//std::cout<<c2<<endl;
		TH2D* hh1 = new TH2D(c2, c2, 80, llim, ulim, 20, ptmin,ptmax);
                
                TProfile *tpf = hh1->ProfileX("",0,100);
                TH1D *h1 = hh1->ProjectionX();
                TH1D *h2 = hh1->ProjectionY();		

                hh1->GetXaxis()->SetTitle(c2);
		hh1->GetYaxis()->SetTitle(" {E}");
                hh1->GetZaxis()->SetTitle(" {N}");
		hh1->GetXaxis()->SetLabelSize(0.05);
		hh1->GetYaxis()->SetLabelSize(0.05);
		hh1->GetXaxis()->SetTitleSize(0.05);
		hh1->SetMinimum(0);
		hh1-> SetLineColor(2);
		h_corels[M][B][C]= hh1;
	}}}

		
	
	
	TH1D *hist1 = new TH1D("h1", "evct", 2, 0, 2);
	hist1->GetXaxis()->SetTitle("before and after cuts");
	hist1->GetYaxis()->SetTitle("Number of evts ");
	hist1->GetXaxis()->SetLabelSize(0.05);
	hist1->GetYaxis()->SetLabelSize(0.05);
	hist1->GetXaxis()->SetTitleSize(0.05);
	hist1->SetMinimum(0);
	hist1-> SetLineColor(2);
	
	TH1D *hist2 = new TH1D("h2", "MDtag-Mass", 400, 100, 500);
	hist2->GetXaxis()->SetTitle("M2");
	hist2->GetYaxis()->SetTitle(" #frac{dN}{dM} [GeV^{-1}]");
	hist2->GetXaxis()->SetLabelSize(0.05);
	hist2->GetYaxis()->SetLabelSize(0.05);
	hist2->GetXaxis()->SetTitleSize(0.05);
	hist2->SetMinimum(0);
	hist2-> SetLineColor(2);
	
	TH1D *hist3 = new TH1D("h3", "a-Mass", 400, 100, 500);
	hist3->GetXaxis()->SetTitle("M3");
	hist3->GetYaxis()->SetTitle(" #frac{dN}{dM} [GeV^{-1}]");
	hist3->GetXaxis()->SetLabelSize(0.05);
	hist3->GetYaxis()->SetLabelSize(0.05);
	hist3->GetXaxis()->SetTitleSize(0.05);
	hist3->SetMinimum(0);
	hist3-> SetLineColor(2);
 
/*       
        TH1D *hist3 = new TH1D("h4", "Z'", 400, 100, 500);
	hist3->GetXaxis()->SetTitle("M3");
	hist3->GetYaxis()->SetTitle(" #frac{dN}{dM} [GeV^{-1}]");
	hist3->GetXaxis()->SetLabelSize(0.05);
	hist3->GetYaxis()->SetLabelSize(0.05);
	hist3->GetXaxis()->SetTitleSize(0.05);
	hist3->SetMinimum(0);
	hist3-> SetLineColor(2);
*/
	
	TH2D *hist21 = new TH2D("hist21", "EC_2_13", 30, llim, ulim, 30, llim, ulim);
	hist21->GetXaxis()->SetTitle("E_C_2_1");
	hist21->GetYaxis()->SetTitle("E_C_2_3");
	hist21->GetZaxis()->SetTitle("# of evts.");
	hist21->GetXaxis()->SetLabelSize(0.05);
	hist21->GetYaxis()->SetLabelSize(0.05);
	hist21->GetXaxis()->SetTitleSize(0.05);
	hist21->SetMinimum(0);
	hist21-> SetLineColor(2);
	
	TH2D *pthist23 = new TH2D("pthist23", "pTC_2_23", 30, llim, ulim, 30, llim, ulim);
	pthist23->GetXaxis()->SetTitle("E_C_2_2");
	pthist23->GetYaxis()->SetTitle("E_C_2_3");
	pthist23->GetZaxis()->SetTitle("# of evts.");
	pthist23->GetXaxis()->SetLabelSize(0.05);
	pthist23->GetYaxis()->SetLabelSize(0.05);
	pthist23->GetXaxis()->SetTitleSize(0.05);
	pthist23->SetMinimum(0);
	pthist23-> SetLineColor(2);
	
	TH2D *pthist13 = new TH2D("pthist13", "pTC_2_13", 30, llim, ulim, 30, llim, ulim);
	pthist13->GetXaxis()->SetTitle("E_C_2_1");
	pthist13->GetYaxis()->SetTitle("E_C_2_3");
	pthist13->GetZaxis()->SetTitle("# of evts.");
	pthist13->GetXaxis()->SetLabelSize(0.05);
	pthist13->GetYaxis()->SetLabelSize(0.05);
	pthist13->GetXaxis()->SetTitleSize(0.05);
	pthist13->SetMinimum(0);
	pthist13-> SetLineColor(2);
	
	TH2D *pthist12 = new TH2D("pthist12", "pTC_2_12", 30, llim, ulim, 30, llim, ulim);
	pthist12->GetXaxis()->SetTitle("E_C_2_1");
	pthist12->GetYaxis()->SetTitle("E_C_2_2");
	pthist12->GetZaxis()->SetTitle("# of evts.");
	pthist12->GetXaxis()->SetLabelSize(0.05);
	pthist12->GetYaxis()->SetLabelSize(0.05);
	pthist12->GetXaxis()->SetTitleSize(0.05);
	pthist12->SetMinimum(0);
	pthist12-> SetLineColor(2);


        TH3D *pthist123 = new TH3D("pthist123", "pTC_2_123", 30, llim, ulim, 30, llim, ulim, 30, llim, ulim);
        pthist123->GetXaxis()->SetTitle("E_C_2_1");
        pthist123->GetYaxis()->SetTitle("E_C_2_2");
        pthist123->GetZaxis()->SetTitle("E_C_3_2");
        pthist123->GetXaxis()->SetLabelSize(0.05);
        pthist123->GetYaxis()->SetLabelSize(0.05);
        pthist123->GetXaxis()->SetTitleSize(0.05);
        pthist123->SetMinimum(0);
        pthist123-> SetLineColor(2);



	
	
//	f.Write();
//	f.Close();
//	exit(1);
	
	// Generator.
	Pythia pythia;
	
	//pythia.particleData.listAll(); 
	
        //exit(1);
	// Read in subrun-independent data from input.cmnd.
	pythia.readFile( "input.cmnd", 0);

	// Extract data to be used in main program. Set counters.
	int nSubrun = pythia.mode("Main:numberOfSubruns");
	int nAbort  = pythia.mode("Main:timesAllowErrors");
	int iAbort  = 0;
	
	int totevents= 0;



	  

	// Begin loop over subruns.
	for (int iSubrun = 1; iSubrun <= nSubrun; ++iSubrun) {

		// Read in subrun-specific data from input.cmnd.
		pythia.readFile( "input.cmnd", iSubrun);

		// Initialize generator.
		//pythia.init();
		


	        // Create UserHooks pointer. Stop if it failed. Pass pointer to Pythia.
                //CombineMatchingInput combined;
	        //UserHooks* matching = combined.getHook(pythia);
	      /*  JetMatchingMadgraph* matching            = NULL;
                matching = new JetMatchingMadgraph();
                if (!matching) {
                cerr << " Failed to initialise jet matching structures.\n"
                 << " Program stopped.";
                 return 1;
                }
                pythia.setUserHooksPtr(matching);
	  */
	        // Initialise Pythia.
	        if (!pythia.init()) {
	            cout << "Error: could not initialise Pythia" << endl;
	            return 1;
	        };
		// Create and open file for LHEF 3.0 output.
		LHEF3FromPythia8 myLHEF3(&pythia.event, &pythia.settings, &pythia.info,
		&pythia.particleData);
		myLHEF3.openLHEF("selected_events.lhe");

		// Write out initialization info on the file.
		myLHEF3.setInit();


	        
		time_t start = time(0);	

		// Begin infinite event loop - to be exited at end of file.

		for (int iEvent = 0; ; ++iEvent) {
//	         for (int iEvent = 0; iEvent < 100000 ; ++iEvent) {		
                    if ( iEvent%5000==0 ) {
  
				std::cout << "Processing Event Number " << iEvent 
				<<",	 time : [" << difftime( time(0), start)<<"s]."
					   << std::endl;
				   }
					   
			//Leave after # of events
                        if (iEvent>50000) break;

			// Generate next event.
			if (!pythia.next()) {

				// Leave event loop if at end of file.
				if (pythia.info.atEndOfFile()) break;

				// First few failures write off as "acceptable" errors, then quit.
				if (++iAbort < nAbort) continue;
				break;
			}
			// Filter particles for jets
			vector<PseudoJet> jetcti; // jet constituents
			IsJetC isjetcon;
			//Loop over events
			for (int i = 0; i < pythia.event.size(); ++i){
				if (isjetcon(&pythia.event[i])){ 
					PseudoJet jetpart(pythia.event[i].px(),pythia.event[i].py(),pythia.event[i].pz(),pythia.event[i].e());
                          		jetcti.push_back(jetpart);
					//std::cout<<pythia.event[i].id()<<"\t"<<pythia.event[i].pT()<<"\t"<<pythia.event[i].eta()<<endl;
				}
			}//(int i = 0; i < pythia.event.size(); ++i) //Loop over events
			

			//vector <PseudoJet> subjets = SmearedJets(jetcti); // Smear the Jets
			vector <PseudoJet> subjets = jetcti; // Don't Smear the Jets
 //==========================                     
 			
//                        PseudoJet fjet= fatjets(subjets);
//			hist1-> Fill(fjet.m());
 
                        // Check if the event satisfied massdrop criteria	
	
			PseudoJet resonance= MDTAG(subjets);
			hist1->Fill(0.5);
			if(resonance.m()< 100 || resonance.m()>150) continue;// Check if Mass drop tagger passes
                        //if(resonance.pt() < 500 ) continue;
                        vector<PseudoJet> hardest_subjets;
                        for(int ii=0; ii< 4; ii++){
                          hardest_subjets.push_back(subjets[ii]);
                        }
			//PseudoJet fjet = fatjets(subjets, resonance);
			PseudoJet fjet = resonance;
                        hist1-> Fill(1.5);
			hist2-> Fill(resonance.m());
			hist3-> Fill(fjet.m()); 
			
			// Store and write event info.
			myLHEF3.setEvent();



//                        cout << "resonance mass " << resonance.m() << endl;
//                        cout << " fjet mass " << fjet.m() << endl;
//                        cout << "resonance pt " << resonance.pt() << endl;
//                        cout << "fjet pt " << fjet.pt() << endl;


//=====Redefining input to correlators using anti_kt jets


// Input to correlators===================                     
                       	

			Mat3d MCorels=Ecorel(jetcti,resonance);
			for (unsigned int M = 0; M < 2; M++) {
                		for (unsigned int B = 0; B < betalist.size(); B++) {
                        		for (unsigned int C = 1; C < 5; C++) {
						h_corels[M][B][C]->Fill(MCorels[M][B][C],fjet.e() );

//                            cout << "EC11 = " <<  MCorels[1][3][1] << " E = "<< fjet.pt() << endl ;	
					}
				}
			}
			//std::cout<<"===================="<<endl;

//                        cout << "EC11,E = " <<  MCorels[1][3][1] << fjet.e() << endl ;	
			hist21-> Fill(MCorels[1][5][1],MCorels[1][5][3]);
			pthist12-> Fill(MCorels[0][5][1],MCorels[0][5][2]);
			pthist13-> Fill(MCorels[0][5][1],MCorels[0][5][3]);
			pthist23-> Fill(MCorels[0][5][2],MCorels[0][5][3]);
                        pthist123-> Fill(MCorels[0][5][1],MCorels[0][5][2],MCorels[0][5][3]);

			
			
		totevents++;


		//pythia.event.list();
		// End of event loop.
		}
	// Write endtag. Overwrite initialization info with new cross sections.
	myLHEF3.closeLHEF(true);

       
         //delete matching;

	// End of subrun loop.
	}

	// Give statistics. Print histogram.
	pythia.stat();
	
     
        f.Write();
	f.Close();

	// Done.
	return 0;
}













